const mongoose = require('mongoose')
const url = 'mongodb://localhost:27017/myapp'
mongoose.connect(url,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    }, (err, res) =>
{
        if (err) {
         return   console.log('Unable to connect')
        }

        console.log('Connected to mongodb')
        
})