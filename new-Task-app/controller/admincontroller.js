const User = require('../model/users')
const sharp = require('sharp')

module.exports = {
    login: async (req, res) =>
    {
        const params = Object.keys(req.body)
        const allowedparams = ['email', 'password']   
        const check = params.every((param) => allowedparams.includes(param))
    
        if (!check) {
            return res.status(422).send({
                "message": "Params required",
            })
        }

        try {
            const user = await User.findByCredentials(req.body.email, req.body.password)
            const token = await user.generateAuthToken() 

            res.status(200).send({
                "message": "Login successfully!",
                "body": user,
                "token":token

            })
        } catch (e) {
            res.status(400).send({
                "message": "Something went wrong!",
                "error": e.message
            })
        }
    },

    register:async (req, res) =>
    {
        try {
            const user = new User(req.body)
            await User.checkUser(req.body.email)
            await user.save()
            
            await user.generateAuthToken()
            
            res.send({
                "message": "User added successfully!",
                "body": user
            })
        } catch (e) {
            res.status(422).send({
                "message":e.message
            })
        }
    },

    getAllUsers:async (req, res) =>
    {
        const user = await User.find({})
        res.send({
            "message": "Details found",
            "body":user
        })
    },

    logout:async (req, res) =>
    {
        try {
            req.user.tokens = req.user.tokens.filter((token) =>
            {
                return token.token !== req.token
            })

            await req.user.save();
            res.status(200).send({
                "message": "Logout Successfully",
            })
        } catch (e) {
            res.status(400).send({
                "message": "Something went wrong",
                "error":e.messages
            })
        }
    },

    getProfile:async (req, res) =>
    {
        const user =  req.user
        
        res.send({
            "message": "Details found",
            "body": user
        })
    },

    userAvtaar: async (req, res) =>
    {
        const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
        req.user.avatar = buffer;
        await req.user.save()
        res.status(200).send()
    },
    
    getUserAvtaar: async (req, res) =>
    {
        try {
            const user =await User.findById(req.params.id)
            if (!user) {
                throw new Error()
            }
            
            res.set('Content-Type','image/png')
            res.send(user.avatar)
        } catch (e) {
            console.log(e)
            res.status(400).send(e.message)
        }    
    },

    deleteUserAvtaar: async (req, res) =>
    {
        req.user.avatar = undefined;
        await req.user.save()
        res.status(200).send()
    },

    logoutAllSessions:async (req, res) =>
    {
        try {
            req.user.tokens = []
            await req.user.save();
            res.status(200).send({
                "message": "Logout Successfully",
            })
        } catch (e) {
            console.log(e)
            res.status(400).send({
                "message": "Something went wrong",
                "error":e.messages
            })
        }
    },

    deleteAccount:async (req, res) =>
    {
        try {
            const user = await req.user.remove()
            res.status(200).send({
                "message":"Your account has been deleted successfully!"
            })    
        } catch (e) {
            res.status(400).send({
                "message": "Something went wrong!",
                "error":e.message
            })
        }
        
    },

    updateProfile:async (req, res) =>
    {
        const params = Object.keys(req.body)
        // console.log(req.body)
        const allowedparams = ['name','email','password']
        const checkparams = params.every((param) => allowedparams.includes(param) )
        if (!checkparams) {
            res.status(422).send({
                "message":"Invalid body params!"
            })
        }
    
        try {

            await User.checkUser(req.body.email)
            params.forEach((param) =>
            {
                return req.user[param] = req.body[param]
            })
            await req.user.save()
                
            res.status(200).send({
                "message": "Profile Updated Successfully",
                "body":req.user
            })

        } catch (e) {
            res.status(200).send({
                "message": "Something went wrong",
                "error": e.message
            })
        }    
    }
}