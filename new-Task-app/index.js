const connection = require('./config/db')
const User = require('./model/users')
const express = require('express')
const app = express()
const path = require('path')
const usersroutes = require('./route/users')
const tasksroutes = require('./route/tasks')
const jwt = require('jsonwebtoken')
const auth = require('./middleware/auth')

app.use(express.json())
app.use(usersroutes)
app.use(auth,tasksroutes)

app.listen(3000, (err, res)=>{
    console.log('connected on 3000')
})
