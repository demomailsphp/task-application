const express = require('express')
const router = express.Router()
const User = require('../model/users')
const auth = require('../middleware/auth')
const adminController = require('../controller/admincontroller')
const multer = require('multer')
const upload = multer({
    limits: {
        fileSize:1000000
    },
    fileFilter(req,file,cb)
    {
        if (!file.originalname.match(/\.(jpg|jpeg|png)/)) {
            cb(new Error('Please provide image which have one of the following extention:(jpg,jpeg,png)'))
        }
        cb(undefined,true)
    }
})  

const expressError = (err, req, res, next) =>
{
        res.status(400).send({
            message:err.message
        })
}
    
router.post('/users', adminController.register)

router.post('/login', adminController.login )

router.get('/users/me', auth, adminController.getProfile)

router.post('/users/me/avatar', [auth, upload.single('avatar')], adminController.userAvtaar, expressError)

router.delete('/users/me/avatar', [auth], adminController.deleteUserAvtaar)

router.get('/users/:id/avatar', adminController.getUserAvtaar)

router.get('/users', auth , adminController.getAllUsers)

router.post('/logout', auth, adminController.logout)

router.post('/logout/all', auth, adminController.logoutAllSessions)

router.delete('/users/me', auth, adminController.deleteAccount)

router.post('/users/me/update', auth, adminController.updateProfile)

module.exports = router;