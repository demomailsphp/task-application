const express = require('express')
const router = express.Router()
const User = require('../model/users') 

const auth = require('../middleware/auth')

const Task = require('../model/tasks')


router.post('/tasks', async (req, res) =>
{
    const task = new Task({
        ...req.body,
        user_id:req.user._id
    })

    try {
        await task.save()
        res.status(200).send({
            message: "Task Added Successfully",
            body:task
        })
    } catch (e) {
        res.status(200).send({
            message: "Something went wrong",
            body:e.message
        })
    }

    
})

router.get('/tasks/:id',  async (req, res) => {
    const _id = req.params.id

    try {
        //const task = await Task.findById(_id)
        
        const task = await Task.findOne({
            _id,
            user_id:req.user._id
        })  

        if (!task) {
            throw new Error('No tasks found related to user which is login')
        }
        res.status(200).send({
            message: "Details Found",
            body:task
        })    
    } catch (e) {
        res.status(400).send({
            message: "Something went wrong!",
            body:e.message
        })
    }
    
    
})

//GET tasks accoding to query string options
//GET completed=false
//GET limit=2&skip=10
router.get('/tasks',  async (req, res) =>
{
    const match = {}
    const sort = {}

    if (req.query.completed) {
        match.is_completed = req.query.completed === 'true'
    }

    if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        console.log(parts)
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
    }
    
    try {
        await req.user.populate({
            path: 'tasks',
            match: match,
            options: {
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort
            }
        }).execPopulate()
        res.status(200).send({
            message: "Details Found",
            body:req.user.tasks
            // body:data
        })
    } catch (e) {
        res.status(400).send({
            message: "Something went wrong!",
            body:e.message
        })
    }
})

router.patch('/tasks/:id',  async (req, res) =>
{
    const _id = req.params.id

    const params = Object.keys(req.body)
    const allowedparams = ['description', 'is_completed']   
    const check = params.every((param) => allowedparams.includes(param))
    console.log(check)
    if (!check) {
        return res.status(422).send({
            "message": "Params required",
        })
    }

    try{
        const task = await Task.findOne({ _id, user_id: req.user._id })
        if (!task) {
            throw new Error('Task not found')
        }

        params.forEach((param) =>
        {
            task[param] =  req.body[param] 
        })

        await task.save()

        res.status(200).send({
            message: "Details Found",
            body:task
        })

    } catch (e) {
        res.status(400).send({
            message: "Something went wrong!",
            body:e.message
        })
    }
})

router.delete('/tasks/:id',  async (req, res) =>
{
    try {
        const task = await Task.findOneAndDelete({_id: req.params.id, user_id: req.user._id})
        if (!task) {
            throw new Error('Task not found')
        }

        res.status(200).send({
            message: "Task deleted successfully!",
        })

    } catch (e) {
        res.status(400).send({
            message: "Something went wrong!",
            body:e.message
        })
    }
})

module.exports = router