const mongoose = require('mongoose')

const taskSchema = mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'User'
    },
    description: {
        type: String,
        required: true,
        trim:true
    },
    is_completed: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true,
})

taskSchema.virtual('users', {
    ref: 'User',    // model name 
    localField: 'user_id',  //localid
    foreignField:'_id' //foreign key
})

taskSchema.set('toObject', { virtuals: true });
taskSchema.set('toJSON', { virtuals: true });

const Task = mongoose.model('Task',taskSchema)

module.exports = Task
