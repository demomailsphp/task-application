const mongoose = require('mongoose')
const validate = require('validator')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Task = require('./tasks')

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        validate(value){
            return validate.isEmail(value)
        },
        runValidators: true,
    },
    password: {
        type: String,
        required: true,
    },
    avatar: {
        type: Buffer
    },
    tokens: [{
        token: {
            type: String,
            required: true,            
        }
    }]
}, {
    timestamps: true,
})

userSchema.virtual('tasks', {
    ref: 'Task',    // model name 
    localField: '_id',  //localid
    foreignField:'user_id' //foreign key
})

userSchema.set('toObject', { virtuals: true });
userSchema.set('toJSON', { virtuals: true });

userSchema.methods.toJSON =  function () 
{
    const user = this
    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens
    delete userObject.avatar
    // console.log(userObject)
    return userObject
    
}

userSchema.statics.checkUser = async (email) => 
{
    const user = await User.findOne({email}) 
    if (user) {
        throw new Error('User email exits.')
    }
}

userSchema.methods.generateAuthToken = async function ()
{
    const user = this
    const token =  jwt.sign({_id:user._id.toString()}, 'thisismynewcourse')

    user.tokens = user.tokens.concat({ token })
    user.save()
    return token  
}

userSchema.statics.findByCredentials = async (email,password) => 
{
    const user = await User.findOne({ email })
    if (!user) {
        throw new Error('User doesn`t exits.')
    }

    const compare = await bcrypt.compare(password, user.password)
    if (!compare) {
        throw new Error('Password is not correct.')
    }

    return user;
}

//hash password when change or create new user
userSchema.pre('save',async function(next) 
{
    const user = this
    if (user.isModified('password')) {
        const password = await bcrypt.hash(user.password, 8)  
        user.password = password
    }
    next()
}) 

//delete tasks also when user is deleted

userSchema.pre('remove', async function (next)
{
    const user = this
    await Task.deleteMany({ user_id: user._id })

    next()
})


const User = mongoose.model('User', userSchema)

module.exports = User