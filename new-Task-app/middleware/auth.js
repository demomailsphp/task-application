const jwt = require('jsonwebtoken')
const User = require('../model/users')  
const auth = async (req, res, next) =>
{
    try {
        const token = req.header('Authorization').replace('Bearer ','')
        const decorded = jwt.verify(token, 'thisismynewcourse')
        // console.log(decorded)
        const user = await User.findOne({ _id:decorded._id, 'tokens.token':token})
            // console.log(user)
        if (!user) {
            throw new Error()
        }

        req.token = token
        req.user = user
        next()

    } catch (e) {
        res.status(401).send({
            "message": "Unauthentication",
            // "error" : e.message
        })        
    }
}

module.exports = auth